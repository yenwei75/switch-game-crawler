from bs4 import BeautifulSoup
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# from selenium.common.exceptions import TimeoutException
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
import sys
import xlwt

gameRecord = {}  # 單筆遊戲record
gameRecords = []  # 全部遊戲record

desired_capabilities = DesiredCapabilities.CHROME  # 修改页面加载策略
# 注释这两行会导致最后输出结果的延迟，即等待页面加载完成再输出
desired_capabilities["pageLoadStrategy"] = "normal"
option = webdriver.ChromeOptions()
option.add_argument("headless")  # 啟用無痕模式 註解此行會打開browser模擬user動作。
driver = webdriver.Chrome(executable_path="./chromedriver",
                          chrome_options=option, desired_capabilities=desired_capabilities)

def getGameData():  # 取得遊戲資料
    print("正在連到網站https://eshoplist.gameqb.net/")
    driver.get("https://eshoplist.gameqb.net/")
    html_doc = driver.find_element_by_css_selector(
        "section").get_attribute('innerHTML')

    soup = BeautifulSoup(html_doc, 'html.parser')
    pricebox = soup.select("#pricebox")  # 取出每個遊戲pricebox
    # print(pricebox)
    print("開始抓取遊戲資料")
    for gameData in pricebox:  # 遍歷每個pricebox 丟進dictionary
        c_name = gameData.select("#c_name")
        pricetip = gameData.select("#pricetip")
        lower_contry = gameData.select("#lower_contry")
        lower_price = gameData.select("#lower_price")
        higher_contry = gameData.select("#higher_contry")
        higher_price = gameData.select("#higher_price")

        # 丟進字典
        gameRecord['c_name'] = checkIsEmpty(c_name)  # 遊戲名稱
        gameRecord['pricetip'] = checkIsEmpty(pricetip)[3:]  # 與最高價地區價差高達
        gameRecord['lower_contry'] = checkIsEmpty(lower_contry)  # 最低價格城市
        gameRecord['lower_price'] = checkIsEmpty(lower_price)  # 最低價格
        gameRecord['higher_contry'] = checkIsEmpty(higher_contry)  # 最高價格城市
        gameRecord['higher_price'] = checkIsEmpty(higher_price)  # 最高價格
        # print(str(gameRecord.items()))
        gameRecords.append(gameRecord.copy())  # 將每筆gameRecord丟入array
    print("開始將資料寫入EXCEL")
    writeToExcel(gameRecords)  # 將資料寫入EXCEL


def writeToExcel(gameRecords):  # 將資料寫入EXCEL
    gameRecords = sorted(
        gameRecords, key=lambda k: k['pricetip'], reverse=True)
    # 寫入excel第1列欄位名稱
    book = xlwt.Workbook(encoding='utf-8', style_compression=0)
    sheet = book.add_sheet('Switch E-Shop 地區比價網', cell_overwrite_ok=True)
    sheet.write(0, 0, "遊戲名稱")
    sheet.write(0, 1, "與最高價地區價差高達")
    sheet.write(0, 2, "最低價格城市")
    sheet.write(0, 3, "最低價格")
    sheet.write(0, 4, "最高價格城市")
    sheet.write(0, 5, "最高價格")
    # 寫入每筆gameRecord
    for index, gameRecord in enumerate(gameRecords):
        # print(str(gameRecord)+"，")
        sheet.write(index+1, 0, gameRecord.get("c_name"))
        sheet.write(index+1, 1, gameRecord.get("pricetip"))
        sheet.write(index+1, 2, gameRecord.get("lower_contry"))
        sheet.write(index+1, 3, gameRecord.get("lower_price"))
        sheet.write(index+1, 4, gameRecord.get("higher_contry"))
        sheet.write(index+1, 5, gameRecord.get("higher_price"))
    book.save("Switch E-Shop 地區比價網"+str(datetime.now().strftime('%Y_%m_%d %H_%M_%S'))+".xls")
    # print("gameRecords"+str(gameRecords))
    print("執行結束，已產生檔案：Switch E-Shop 地區比價網"+str(datetime.now().strftime('%Y_%m_%d %H_%M_%S'))+".xls")


def checkIsEmpty(variable):
    if variable:  # 有值就取nodeValue
        variable = variable[0].text
    else:
        variable = ""
    return variable


print("getGameData() start:")  # 程式起始位置
getGameData()
# input()
# driver.quit()  # 關閉瀏覽器
